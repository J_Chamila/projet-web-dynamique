-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Client :  127.0.0.1
-- Généré le :  Ven 21 Avril 2017 à 02:37
-- Version du serveur :  5.7.14
-- Version de PHP :  5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `social media 2017`
--

-- --------------------------------------------------------

--
-- Structure de la table `administrateur`
--

CREATE TABLE `administrateur` (
  `ID` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `Password` varchar(40) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `conversation`
--

CREATE TABLE `conversation` (
  `conv_id` int(11) NOT NULL,
  `noms` text NOT NULL,
  `messages` text NOT NULL,
  `nb_membres` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `events`
--

CREATE TABLE `events` (
  `event_id` int(11) NOT NULL,
  `event_nom` varchar(60) NOT NULL,
  `event_lieu` varchar(50) NOT NULL,
  `event_date` date NOT NULL,
  `event_time` time NOT NULL,
  `event_acces` int(11) NOT NULL,
  `event_like` int(4) NOT NULL,
  `event_comment` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `infos`
--

CREATE TABLE `infos` (
  `user_email` varchar(65) NOT NULL,
  `username` varchar(50) NOT NULL,
  `Amis` text NOT NULL,
  `a_propos` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `mur`
--

CREATE TABLE `mur` (
  `user_nom` varchar(50) NOT NULL,
  `user_email` varchar(65) NOT NULL,
  `photo_profil` varchar(60) NOT NULL,
  `photo_couv` varchar(60) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `photos`
--

CREATE TABLE `photos` (
  `photo_id` int(11) NOT NULL,
  `photo_nom` varchar(50) NOT NULL,
  `photo_lieu` varchar(50) NOT NULL,
  `photo_date` date NOT NULL,
  `photo_time` time NOT NULL,
  `photo_taille` varchar(25) NOT NULL,
  `photo_type` varchar(25) NOT NULL,
  `photo_like` int(4) NOT NULL,
  `photo_acces` int(11) NOT NULL,
  `photo_comment` varchar(1000) NOT NULL,
  `photo_activite` varchar(100) NOT NULL,
  `photo_sentiment` varchar(50) NOT NULL,
  `photo_blob` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `user_id` int(11) NOT NULL,
  `Username` varchar(50) NOT NULL,
  `nom` varchar(50) NOT NULL,
  `prenom` varchar(50) NOT NULL,
  `date` date NOT NULL,
  `Password` varchar(40) NOT NULL,
  `Email` varchar(65) NOT NULL,
  `Mur` blob NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `user`
--

INSERT INTO `user` (`user_id`, `Username`, `nom`, `prenom`, `date`, `Password`, `Email`, `Mur`) VALUES
(1, 'nazimo', 'Timsiline', 'Nazim', '1997-02-26', 'lol', 'n.timsiline@hotmail.fr', ''),
(2, 'nazimo', 'Timsiline', 'Nazim', '1997-02-26', 'lol', 'n.timsiline@hotmail.fr', ''),
(3, 'nazimo', 'Timsiline', 'Nazim', '1997-02-26', 'lol', 'n.timsiline@hotmail.fr', 0x30),
(4, 'nazimo', 'Timsiline', 'Nazim', '1997-02-26', 'lol', 'n.timsiline@hotmail.fr', 0x30);

-- --------------------------------------------------------

--
-- Structure de la table `videos`
--

CREATE TABLE `videos` (
  `video_id` int(11) NOT NULL,
  `video_nom` varchar(60) NOT NULL,
  `video_lieu` varchar(60) NOT NULL,
  `video_date` date NOT NULL,
  `video_time` time NOT NULL,
  `video_sentiment` varchar(40) NOT NULL,
  `video_activite` varchar(50) NOT NULL,
  `video_acces` int(11) NOT NULL,
  `video_like` int(4) NOT NULL,
  `video_comment` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `administrateur`
--
ALTER TABLE `administrateur`
  ADD PRIMARY KEY (`ID`);

--
-- Index pour la table `conversation`
--
ALTER TABLE `conversation`
  ADD PRIMARY KEY (`conv_id`);

--
-- Index pour la table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`event_id`,`event_nom`);

--
-- Index pour la table `mur`
--
ALTER TABLE `mur`
  ADD PRIMARY KEY (`user_nom`);

--
-- Index pour la table `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`photo_id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`user_id`,`Email`);

--
-- Index pour la table `videos`
--
ALTER TABLE `videos`
  ADD PRIMARY KEY (`video_id`,`video_nom`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `administrateur`
--
ALTER TABLE `administrateur`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `events`
--
ALTER TABLE `events`
  MODIFY `event_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `photos`
--
ALTER TABLE `photos`
  MODIFY `photo_id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `user_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT pour la table `videos`
--
ALTER TABLE `videos`
  MODIFY `video_id` int(11) NOT NULL AUTO_INCREMENT;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
